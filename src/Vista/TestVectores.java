/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestVectores {

    public static void main(String[] args) {
        
        ListaNumeros lista = new ListaNumeros(leerEntero("Digite cantidad de elementos para la lista1 "));
        Scanner sc = new Scanner (System.in);
        /**
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         *  Ejemplo: this={3,4,5,6} y dos={3,4}
         *  ListaNueva = {3,4,5,6}
         * lista1
         */
        for (int i = 0; i < lista.length(); i++) {
            lista.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }
        System.out.println("Su lista1 es: " + lista.toString());
        
        lista.ordenar_Seleccion();
        System.out.println("Su lista ordenada con seleccion es: " + lista.toString());
        
        lista.ordenar_Burbuja();
        System.out.println("Su lista ordenada con burbuja es: " + lista.toString());
        
        
        ListaNumeros lista2 = new ListaNumeros(leerEntero("Digite cantidad de elementos para la lista2 "));
        
        for (int i = 0; i < lista2.length(); i++) {
            lista2.adicionar(i, leerFloat("Digite dato [" + i + "]:"));
        }
        System.out.println("Su lista2 es: " + lista2.toString());
        
        lista2.ordenar_Seleccion();
        System.out.println("Su lista2 ordenada con seleccion es: " + lista2.toString());
        
        lista2.ordenar_Burbuja();
        System.out.println("Su lista2 ordenada con burbuja es: " + lista2.toString());
        
        System.out.println("Su lista unida es: " + lista.getUnion(lista2));
        
        System.out.println("Su interseccion es: " + lista.getInterseccion(lista2));
        
        System.out.println("Digite la posisicon que desea eliminar ");
        lista.eliminar(sc.nextInt());
        System.out.println("Su nuevo vector es: "+lista.toString());
        
        if (lista.compareTo(lista2) == 0){
            System.out.println("La lista1 es mayor a la lista2");
        } else {
            System.out.println("La lista1 es menor a la lista2");
        }
        
    }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Entero");
            return leerEntero(msg);
        }

    }

}
